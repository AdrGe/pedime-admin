// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  fbConfig: {
    host: 'localhost:8080',
    ssl: false,
    apiKey: 'AIzaSyC4Ph1n68M-Ga1Z4v_uzlnfQoeUgqZ8KNQ',
    authDomain: 'pedime-app.firebaseapp.com',
    databaseURL: 'http://localhost:9000?ns=pedime-app.firebaseio.com',
    projectId: 'pedime-app',
    storageBucket: 'pedime-app.appspot.com',
    messagingSenderId: '45484855909',
    appId: '1:45484855909:web:a84b9b21724aee741e6c8c',
    measurementId: 'G-38ECW8768Q',
  },
};
/* export const environment = {
  production: false,
  firebaseConfig: {
      host: 'localhost:8081',
      ssl: false,
      apiKey: '<api-key>',
      authDomain: '<project-id>.firebaseapp.com',
      databaseURL: 'http://localhost:9000?ns=<project-id>',
      projectId: '<project-id>',
      appId: '<app-id>',
      measurementId: '<measurement-id>',
  },
}; */
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/dist/zone-error'; // Included with Angular CLI.
