import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { HomeComponent } from './home/home.component';
import { AdminComComponent } from './admin-com/admin-com.component';
import { TopbarComponent } from './topbar/topbar.component';
import { FooterComponent } from './footer/footer.component';
import { PpComponent } from './pp/pp.component';
import { TosComponent } from './tos/tos.component';
import { NvoComerComponent } from './nvo-comer/nvo-comer.component';
import { SnackBarComponent } from './snack-bar/snack-bar.component';
import { ProdctsComponent } from './prodcts/prodcts.component';
import { NvoProdComponent } from './nvo-prod/nvo-prod.component';
import { SubeImagenComponent } from './sube-imagen/sube-imagen.component';
import { FaqComponent } from './faq/faq.component';
import { AuthPageComponent } from './auth-page/auth-page.component';
import { PreparaComponent } from './prepara/prepara.component';
import { NvoUserComponent } from './nvo-user/nvo-user.component';
import { GorraComponent } from './gorra/gorra.component';

import { FileUploadModule } from '@iplab/ngx-file-upload';
import { OverlayModule } from '@angular/cdk/overlay';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatChipsModule } from '@angular/material/chips';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatStepperModule } from '@angular/material/stepper';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgxAuthFirebaseUIModule } from 'ngx-auth-firebaseui';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {
  NgcCookieConsentModule,
  NgcCookieConsentConfig,
} from 'ngx-cookieconsent';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/', '.json');
}

const cookieConfig: NgcCookieConsentConfig = {
  cookie: {
    domain: 'gzsoft.com',
  },
  position: 'bottom-right',
  theme: 'edgeless',
  palette: {
    popup: {
      background: '#000000',
      text: '#ffffff',
      link: '#ffffff',
    },
    button: {
      background: '#f1d600',
      text: '#000000',
      border: 'transparent',
    },
  },
  type: 'opt-in',
  content: {
    message:
      'Todos los meses necesitamos de tu apoyo para seguir creciendo y ayudarte a vender.',
    dismiss: 'Aportar',
    deny: 'Este mes aporté',
    link: 'Modos de Aporte',
    href: '/aportes',
    policy: 'Cookie Policy',
  },
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AdminComComponent,
    TopbarComponent,
    FooterComponent,
    PpComponent,
    TosComponent,
    ProdctsComponent,
    NvoProdComponent,
    NvoComerComponent,
    SnackBarComponent,
    SubeImagenComponent,
    FaqComponent,
    AuthPageComponent,
    NvoUserComponent,
    GorraComponent,
    PreparaComponent,
  ],
  imports: [
    ImageCropperModule,
    OverlayModule,
    BrowserModule,
    FlexLayoutModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.fbConfig),
    AngularFirestoreModule.enablePersistence(),
    AngularFireAuthModule,
    NgxAuthFirebaseUIModule.forRoot(environment.fbConfig),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
    NgcCookieConsentModule.forRoot(cookieConfig),
    HttpClientModule,
    AngularFireStorageModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatStepperModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatInputModule,
    MatDialogModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    MatChipsModule,
    MatCheckboxModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    FileUploadModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
