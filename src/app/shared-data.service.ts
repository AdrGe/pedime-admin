import { Injectable } from '@angular/core';
import { userData } from './models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SharedDataService {
  // Para Todos
  public ciudad: string;
  public userId: string;
  public user: userData;
  algo: Observable<userData>;

  // Para Imagenes
  public archSube: string;
  public archivo: File;
  // Para nuevos...
  public nvoProd: boolean = true;
  public idProducto: string;
  public nvoComer: boolean = false;

  constructor() {}

  rubros() {
    return `${this.ciudad}/pedime/rubros`;
  }
  get comers() {
    return 'comercios';
  }
  comercios(): string {
    return `${this.ciudad}/pedime/comercios`;
  }

  docRubro(rubroId: string): string {
    return `${this.ciudad}/pedime/rubros/${rubroId}`;
  }

  comXrubro(rubroId: string): string {
    return `${this.ciudad}/pedime/rubros/${rubroId}/comercios`;
  }

  docComercio(comId: string): string {
    return `${this.ciudad}/pedime/comercios/${comId}`;
  }

  prodsDcomercio(comId: string): string {
    return `${this.ciudad}/pedime/comercios/${comId}/productos`;
  }

  docProducto(comId: string, prodId: string): string {
    return `${this.ciudad}/pedime/comercios/${comId}/productos/${prodId}`;
  }

  storageRef(comId: string): string {
    return `comercios/${comId}`;
  }
  /*   user(comId: string): string {
    return `${this.ciudad}/pedime/comercios/${comId}/user`;
  } */
}
