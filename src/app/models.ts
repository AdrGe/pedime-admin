// TODO: que no sean opcionales los campos

export interface Comercio {
  nombre?: string;
  calle?: string;
  nro?: number;
  tel?: string;
  cel?: string;
  metPago?: [string];
  metDelivery?: [string];
  rubro?: string;
  pedirPor?: [string];
  logo?: string;
}

export interface Rubros {
  rubro: string;
  comercios: Comercio[];
}

export interface Producto {
  nombre?: string;
  precio?: number;
  desc?: string;
  img?: string;
}

export interface ProductCategory {
  categ: string;
  productos: Producto[];
}
export interface userData {
  uid: string;
  email: string;
  displayName: string;
  photoURL: string;
  emailVerified: boolean;
  ciudad?: string;
}
