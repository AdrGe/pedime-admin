import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss'],
})
export class TopbarComponent {
  loggedIn: boolean = false;

  constructor(private auth: AngularFireAuth) {
    if (this.auth.currentUser == null) {
      this.loggedIn = false;
    } else {
      this.loggedIn = true;
    }
    /* let usr = this.auth.getRedirectResult. .currentUser;
    usr.then((data) => data.is)
    this.auth.(function(user) {
      if (user.is) {
        // User is signed in.
      } else {
        // No user is signed in.
      }
    } */
  }
}
