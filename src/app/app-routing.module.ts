import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PpComponent } from './pp/pp.component';
import { AdminComComponent } from './admin-com/admin-com.component';
import { TosComponent } from './tos/tos.component';
import { ProdctsComponent } from './prodcts/prodcts.component';
import { NvoProdComponent } from './nvo-prod/nvo-prod.component';
import { NvoComerComponent } from './nvo-comer/nvo-comer.component';
import {
  AngularFireAuthGuard,
  redirectUnauthorizedTo,
  redirectLoggedInTo,
} from '@angular/fire/auth-guard';
import { SubeImagenComponent } from './sube-imagen/sube-imagen.component';
import { FaqComponent } from './faq/faq.component';
import { AuthPageComponent } from './auth-page/auth-page.component';
import { GorraComponent } from './gorra/gorra.component';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);
const redirectLoggedInToAdmin = () => redirectLoggedInTo(['admin-com']);

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' }, // redirect to `first-component`
  // { path: '**', component: HomeComponent },
  { path: 'home', component: HomeComponent, data: { title: 'PediMe' } },
  {
    path: 'pp',
    component: PpComponent,
    data: { title: 'Políticas de Privacidad' },
  },
  {
    path: 'faq',
    component: FaqComponent,
  },
  {
    path: 'admin-com',
    component: AdminComComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'sube',
    component: SubeImagenComponent,
  },
  {
    path: 'tos',
    component: TosComponent,
    data: { title: 'Términos del Servicio' },
  },
  {
    path: 'prods',
    canActivate: [AngularFireAuthGuard],
    component: ProdctsComponent,
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'nvoProd',
    canActivate: [AngularFireAuthGuard],
    component: NvoProdComponent,
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'nc',
    component: NvoComerComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  },
  {
    path: 'aportes',
    component: GorraComponent,
  },
  {
    path: 'authPage',
    component: AuthPageComponent,
    data: { authGuardPipe: redirectLoggedInToAdmin },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
