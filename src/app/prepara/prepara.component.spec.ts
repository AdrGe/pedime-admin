import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreparaComponent } from './prepara.component';

describe('PreparaComponent', () => {
  let component: PreparaComponent;
  let fixture: ComponentFixture<PreparaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreparaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreparaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
