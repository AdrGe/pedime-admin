import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SharedDataService } from '../shared-data.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirestoreService } from '../firestore.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-nvo-user',
  templateUrl: './nvo-user.component.html',
  styleUrls: ['./nvo-user.component.scss'],
})
export class NvoUserComponent {
  constructor(
    private sds: SharedDataService,
    private router: Router,
    private auth: AngularFireAuth,
    private fs: FirestoreService,
    private dialogRef: MatDialogRef<NvoUserComponent>
  ) {}

  usuarioViejo() {
    this.auth.user.subscribe((user) => {
      this.sds.user = user;
      this.sds.userId = user.uid;
      this.sds.nvoComer = false;
      this.fs.getComer().subscribe((data) => (this.sds.ciudad = data.ciudad));
    });
    this.router.navigate(['admin-com']);
    this.dialogRef.close();
  }

  usuarioNuevo() {
    this.auth.user.subscribe((user) => {
      this.sds.user = user;
      this.sds.userId = user.uid;
    });
    this.sds.nvoComer = true;
    this.router.navigate(['nc']);
    this.dialogRef.close();
  }
}
