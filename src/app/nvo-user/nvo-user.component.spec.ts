import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NvoUserComponent } from './nvo-user.component';

describe('NvoUserComponent', () => {
  let component: NvoUserComponent;
  let fixture: ComponentFixture<NvoUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NvoUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NvoUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
