import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FirestoreService } from '../firestore.service';
import { Producto } from '../models';
import { MatSnackBar } from '@angular/material/snack-bar';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {
  AngularFireStorage,
  AngularFireStorageReference,
  AngularFireUploadTask,
} from '@angular/fire/storage';
import { SnackBarComponent } from '../snack-bar/snack-bar.component';
import { Router } from '@angular/router';
import { SubeImagenComponent } from '../sube-imagen/sube-imagen.component';
import { MatDialog } from '@angular/material/dialog';

import { SharedDataService } from '../shared-data.service';

@Component({
  selector: 'app-nvo-prod',
  templateUrl: './nvo-prod.component.html',
  styleUrls: ['./nvo-prod.component.scss'],
})
export class NvoProdComponent {
  nvoProdForm = this.fb.group({
    nombre: ['', Validators.required],
    desc: ['', Validators.required],
    precio: ['', Validators.required],
    visible: [true, []],
  });

  public nvoProd: Producto = {};
  public nvo: boolean = true;
  public btnArchivo: String = 'Elija un archivo de imagen';
  imgURL: any;
  ref: AngularFireStorageReference;
  downloadURL: Observable<string>;
  grabando: boolean = false;

  constructor(
    private fb: FormBuilder,
    private fs: FirestoreService,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private router: Router,
    private dialog: MatDialog,
    private sds: SharedDataService
  ) {
    if (!this.sds.nvoProd) {
      this.llenaFormu(this.sds.idProducto);
    }
  }

  openDialog(): void {
    this.sds.archSube = this.nvoProdForm.value.nombre;
    const dialogRef = this.dialog.open(SubeImagenComponent, {
      width: '65%',
      height: '90%',
    });
  }
  //  TODO: siempre hay que subir img?

  imgInputChange(fileInputEvent: any) {
    this.btnArchivo = fileInputEvent.target.files[0].name;
    console.log(fileInputEvent.target.files[0]);
  }

  llenaFormu(prodID: string) {
    this.fs.getProd(prodID).subscribe((p) => {
      this.nvoProdForm.setValue({
        nombre: p.data.nombre,
        desc: p.data.desc,
        precio: p.data.precio,
        visible: p.data.visible,
      });
    });
  }

  openSnackBar(msg: string, icn: string) {
    this.snackBar.openFromComponent(SnackBarComponent, {
      data: { msg, icn },
      duration: 2000,
    });
  }

  onSubmit() {
    this.grabando = true;
    if (this.sds.archSube) {
      this.upload(this.sds.archivo);
    } else this.onUploadData();
  }

  upload(archi: File) {
    //se crea nombre archivo quitando los espacios con una regexp
    const narch = `${archi.name.replace(/ /g, '')}`;
    this.ref = this.storage.ref(`comercios/${this.sds.userId}/${narch}`);
    try {
      this.ref.put(archi).then(() => {
        this.ref.getDownloadURL().subscribe((url) => {
          this.imgURL = url;
          this.openSnackBar('Se guardó exitósamente bien', 'thumb_up_alt');
          this.onUploadData();
        });
      });
    } catch (error) {
      console.log(error);
    }
  }

  onUploadData() {
    //agrega el campo imagen
    this.nvoProd = { ...this.nvoProd, ...this.nvoProdForm.value };
    if (this.imgURL) this.nvoProd.img = this.imgURL;

    if (!this.sds.nvoProd) {
      this.fs.updateProd(this.sds.idProducto, this.nvoProd);
      this.openSnackBar('Se actualizó el producto', 'thumb_up');
    } else {
      this.fs.creaProd(this.nvoProd);
      this.openSnackBar('Se creó el producto', 'thumb_up');
    }
    this.grabando = false;
    this.sds.idProducto = '';
    this.sds.archSube = '';
    this.sds.nvoProd = false;
    this.router.navigate(['prods']); //lo crea en firestore
  }

  /*   QUEDÓ como ejemplo
uploadFile(archi: File) {
    const filePath = `comercios/${this.sds.userId}/`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.ref(filePath).child(archi.name).put(archi);
    // observe percentage changes
    this.uploadPercent = task.percentageChanges();
    // get notified when the download URL is available
    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe(this.onUploadData);
          this.sds.archivo = null;
        })
      )
      .subscribe((pct: { bytesTransferred: any; totalBytes: any }) => {
        // this.resetModel();
        if (pct.bytesTransferred == pct.totalBytes) {
        }
      });
  } */

  resetModel() {
    this.nvoProd = {};
    this.sds.archSube = null;
    this.nvoProdForm.reset();
  }
}
