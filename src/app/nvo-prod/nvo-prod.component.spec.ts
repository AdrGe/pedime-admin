import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NvoProdComponent } from './nvo-prod.component';

describe('NvoProdComponent', () => {
  let component: NvoProdComponent;
  let fixture: ComponentFixture<NvoProdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NvoProdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NvoProdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
