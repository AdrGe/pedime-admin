import { Component, OnInit } from '@angular/core';
import { AuthProvider, Theme, LinkMenuItem } from 'ngx-auth-firebaseui';
import { MatDialog } from '@angular/material/dialog';
import { NvoUserComponent } from '../nvo-user/nvo-user.component';
import { PreparaComponent } from '../prepara/prepara.component';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss'],
})
export class AuthPageComponent implements OnInit {
  themes = Theme;
  providers = AuthProvider;
  links: LinkMenuItem[];
  public ver: boolean = false;

  constructor(private dialog: MatDialog) {}

  printLog() {
    console.log('printlog');
  }

  logueado() {
    this.dialog.open(NvoUserComponent, {
      width: '65%',
      height: '65%',
    });
  }

  toggle() {
    this.ver = !this.ver;
  }

  error() {}

  ngOnInit(): void {
    this.dialog.open(PreparaComponent, { width: '330px', height: '380px' });
    /*     this.links = [
      { icon: 'home', text: 'Home', callback: this.printLog },
      { icon: 'favorite', text: 'Favorite', callback: this.printLog },
      { icon: 'add', text: 'Add', callback: this.printLog },
    ];
  } */
  }
}
