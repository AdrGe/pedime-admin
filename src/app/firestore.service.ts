import { Injectable } from '@angular/core';
import 'firebase/firestore';
import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Producto } from './models';
import { map } from 'rxjs/operators';
import { SharedDataService } from './shared-data.service';

@Injectable({
  providedIn: 'root',
})
export class FirestoreService {
  constructor(private afs: AngularFirestore, private sds: SharedDataService) {}

  //Usuarios
  public getUser(userId: string) {
    return this.afs.collection('users').doc(userId).valueChanges();
  }

  // NO involucran ciudad
  public getRubros() {
    return this.afs
      .collection('rubros', (ref) => ref.orderBy('nombre'))
      .valueChanges();
  }

  // Involucrán ciudad
  public getCiudades() {
    return this.afs
      .collection('ciudades', (ref) => ref.orderBy('nombre'))
      .valueChanges();
  }

  public getComercios() {
    return this.afs
      .collection('comercios', (ref) => ref.orderBy('nombre'))
      .valueChanges();
  }

  public getComer(): Observable<any> {
    return this.afs.collection('comercios').doc(this.sds.userId).valueChanges();
  }

  public getProd(prodID: string): Observable<any> {
    return this.afs
      .collection(`comercios/${this.sds.userId}/productos`)
      .doc(prodID)
      .snapshotChanges()
      .pipe(
        map((a) => {
          const data = a.payload.data();
          const id = a.payload.id;
          return { id, data };
        })
      );
  }
  // Crea un nuevo comercio, la ciudad está dentro de data
  public creaCom(data: any) {
    this.afs
      .collection('comercios')
      .doc(this.sds.userId)
      .set(data) //creo comercio
      .then(() => {
        const cCom = {
          nombre: data.nombre,
          rubro: data.rubro,
          logo: data.logo,
          id: this.sds.userId,
        };
        this.afs
          .collection(`ciudades/${data.ciudad}/comercios`)
          .doc(this.sds.userId)
          .set(cCom);
        this.sds.nvoComer = false;
        let rCom = {
          nombre: data.nombre,
          logo: data.logo,
          id: this.sds.userId,
        };
        this.afs
          .collection(`ciudades/${data.ciudad}/rubros/${data.rubro}/comercios`)
          .doc(this.sds.userId)
          .set(rCom);
        /*.then(() => console.log('___creó__Rubro___'));  set o updt ? */
      })
      .catch((error) => {
        console.error('No se pudo guardar el documento: ', error);
      });
  }
  /*   actCiudad(city: string) {
    console.log('________actciudad__________');
    // actualiza la ciudad del comercio
    this.afs
      .collection(`${city}/pedime/comercios/${this.sds.userId}/user`)
      .add({ nombre: city });
  } */

  // Actualiza un comercio
  public updateCom(data: any) {
    let rCom = {
      nombre: data.nombre,
      logo: data.logo,
      id: this.sds.userId,
      rubro: data.rubro,
    };
    this.afs
      .collection(`ciudades/${this.sds.ciudad}/rubros/${data.rubro}/comercios/`)
      .doc(this.sds.userId)
      .update(rCom);

    this.afs
      .collection(`ciudades/${this.sds.ciudad}/comercios/`)
      .doc(this.sds.userId)
      .update(rCom);

    this.afs.collection('comercios').doc(this.sds.userId).update(data);
  }

  // Borra un comercio
  /*   //Falta borrar en rubros
  public deleteCom(documentId: string) {
    return this.afs
      .collection(`${this.comersPath}/${documentId}`)
      .doc(documentId)
      .delete();
  } */

  public creaProd(data: any) {
    return this.afs
      .collection(`comercios/${this.sds.userId}/productos`)
      .add(data);
  }
  // Trae todos los Productos
  public getProductos() {
    return this.afs
      .collection(`comercios/${this.sds.userId}/productos`, (ref) =>
        ref.orderBy('nombre')
      )
      .snapshotChanges()
      .pipe(
        map((actions) => {
          return actions.map((a) => {
            const data = a.payload.doc.data() as Producto;
            const id = a.payload.doc.id;
            return { id, ...data };
          });
        })
      );
  }

  // Trae uno
  public updateProd(documentId: string, data: any) {
    return this.afs
      .collection(`comercios/${this.sds.userId}/productos`)
      .doc(documentId)
      .set(data);
  }

  public deleteProd(documentId: string) {
    return this.afs
      .collection(`comercios/${this.sds.userId}/productos`)
      .doc(documentId)
      .delete();
  }
}
