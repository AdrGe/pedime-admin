import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable } from '@angular/material/table';
import { Producto } from '../models';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs';
import { FirestoreService } from '../firestore.service';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SnackBarComponent } from '../snack-bar/snack-bar.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SharedDataService } from '../shared-data.service';
import { loggedIn } from '@angular/fire/auth-guard/auth-guard';
@Component({
  selector: 'app-prodcts',
  templateUrl: './prodcts.component.html',
  styleUrls: ['./prodcts.component.scss'],
})
export class ProdctsComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<Producto>;
  // dataSource: MyDataSource;
  displayedColumns = ['nombre', 'desc', 'precio', 'action'];
  myData: MyDataSource;
  userID: string;
  loading: boolean = true;

  constructor(
    private router: Router,
    private afs: AngularFirestore,
    private snackBar: MatSnackBar,
    private fs: FirestoreService,
    private sds: SharedDataService,
    private auth: AngularFireAuth
  ) {
    if (this.auth.authState != null) {
      this.auth.user.subscribe((u) => (this.sds.userId = u.uid));
    }
    this.userID = this.sds.userId;
    this.myData = new MyDataSource(this.afs, this.sds.userId);
  }

  borra(prodID: string) {
    this.fs.deleteProd(prodID);
    this.openSnackBar('Producto borrado', 'thumb_up_alt');
  }

  openSnackBar(msg: string, icn: string) {
    this.snackBar.openFromComponent(SnackBarComponent, {
      data: { msg, icn },
      duration: 2000,
    });
  }

  edita(prodID: string) {
    this.sds.idProducto = prodID;
    this.sds.nvoProd = false;
    this.router.navigate(['nvoProd']);
  }

  ngAfterViewInit() {
    // this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.myData;
  }
}

export class MyDataSource extends DataSource<any> {
  myData: any;
  paginator: MatPaginator;
  // sort: MatSort;
  loading = true;

  constructor(private afs: AngularFirestore, private userId: String) {
    super();
  }
  connect(): Observable<any[]> {
    return this.afs
      .collection(`comercios/${this.userId}/productos`, (ref) =>
        ref.orderBy('nombre')
      )
      .snapshotChanges()
      .pipe(
        map((actions) => {
          return actions.map((a) => {
            const data = a.payload.doc.data() as Producto;
            const id = a.payload.doc.id;
            this.loading = false;
            return { id, ...data };
          });
        })
      );
  }

  disconnect() {
    this.loading = false;
  }
}
/* export class ProdctsComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<Producto>;
  dataSource: ProdctsDataSource;

  displayedColumns = ['nombre', 'desc', 'precio'];
  datatableItems;

  loadingIndicator: boolean;

    resourceItems = [];
  resourceItemColumns: DatatableColumn[] = [];
    keys = [];
  userItems = [];
  userItemColumns = [];
  resourceItems = [];
  resourceItemColumns: DatatableColumn[] = [];

  paginatorConfig: PaginatorConfig = {
    itemsPerPage: 10,
    currentPage: 1,
    maxDisplayedPages: 2,
  };

  sortByColumns = ['nombre', 'precio'];
  hasUnitNumber = false;

  constructor(public afs: AngularFirestore) {
    this.dataSource = new ProdctsDataSource(this.afs);
  }

    public getProds(comId: string) {
    this.loadingIndicator = true;
    this.fs.getProductos(comId).subscribe((data) => {
      this.datatableItems = [...this.datatableItems, ...data];
      console.log(this.datatableItems);
    });
    setTimeout(() => {
      this.loadingIndicator = false;
    }, 1500);
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }

    sortResourceItemsByColumn($event: SortItem) {
    const columnName = $event.name;
    const ascending = $event.isSortedByAscending;

    this.resourceItems.sort((rowItem1, rowItem2) => {
      if (rowItem1[columnName] > rowItem2[columnName]) {
        return ascending ? 1 : -1;
      }

      if (rowItem1[columnName] < rowItem2[columnName]) {
        return ascending ? -1 : 1;
      }
      // names must be equal
      return 0;
    });
  } 
}*/
