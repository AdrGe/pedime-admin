import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SharedDataService } from '../shared-data.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-admin-com',
  templateUrl: './admin-com.component.html',
  styleUrls: ['./admin-com.component.scss'],
})
export class AdminComComponent {
  // public loadingIndicator = false;
  // public comer;
  id: string;

  constructor(
    private router: Router,
    private sds: SharedDataService,
    private auth: AngularFireAuth
  ) {
    if (this.auth.authState != null) {
      this.auth.user.subscribe((u) => (this.sds.userId = u.uid));
    }
  }

  listaProductos() {
    this.router.navigate(['prods']);
  }
  nuevoProducto() {
    this.sds.nvoProd = true;
    this.router.navigate(['nvoProd']);
  }
  editaComercio() {
    this.sds.nvoComer = false;
    this.router.navigate(['nc']);
  }

  /*   ngOnInit(): void {
    // this.comer = this.fs.getComer();
  } */
}
