import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  QueryList,
  AfterViewInit,
} from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ImageCropperModule, ImageCroppedEvent } from 'ngx-image-cropper';

import { SharedDataService } from '../shared-data.service';

@Component({
  selector: 'app-sube-imagen',
  templateUrl: './sube-imagen.component.html',
  styleUrls: ['./sube-imagen.component.scss'],
})
export class SubeImagenComponent implements OnInit {
  // @ViewChild('imgInput') imgInput: ElementRef;

  public fileToUpload: File;
  public btnArchivo: string = 'Archivo de imagen';
  uploadPercent: Observable<number>;
  downloadURL: Observable<string>;
  percentage: Observable<number>;
  progress: { percentage: number } = { percentage: 0 };
  imgURL: any;
  error;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  crop = false;

  constructor(
    public dialogRef: MatDialogRef<SubeImagenComponent>,
    private sds: SharedDataService
  ) {}

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.crop = true;
    this.imgURL = event.base64;
  }
  /*   creaImg(blb: Blob) {
    var archi = new File([blb], this.nvoProdForm.value().nombre, {
      type: 'image/png',
    });
    return archi;
  } */
  b64toBlob(dataURI) {
    var byteString = atob(dataURI.split(',')[1]);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    var archi;

    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return (archi = new File([ab], `${this.sds.archSube}.png`, {
      type: 'image/png',
    }));
  }

  fileCropped(blob: Blob) {
    const archi = new File([blob], `${this.sds.archSube}.png`, {
      type: 'image/png',
    });
    console.log('asd', archi);
    this.sds.archivo = archi;
  }

  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
    window.alert('No se pudo cargar la imagen. Intente nuevamente');
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  echo(): void {
    this.sds.archivo = this.b64toBlob(this.imgURL);
    this.dialogRef.close();
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.btnArchivo = this.fileToUpload.name;
  }

  ngOnInit() {
    /*     this.imgInput.nativeElement.querySelector('#imgInput').click();
    selectFile() { */
    let element: HTMLElement = document.querySelector(
      'input[type="file"]'
    ) as HTMLElement;
    element.click();
  }
}
