import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubeImagenComponent } from './sube-imagen.component';

describe('SubeImagenComponent', () => {
  let component: SubeImagenComponent;
  let fixture: ComponentFixture<SubeImagenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubeImagenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubeImagenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
