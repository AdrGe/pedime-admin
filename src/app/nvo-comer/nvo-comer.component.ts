import { Component, OnInit } from '@angular/core';
import 'firebase/storage';
import * as firebase from 'firebase';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, Validators } from '@angular/forms';
import { FirestoreService } from '../firestore.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarComponent } from '../snack-bar/snack-bar.component';
import { SubeImagenComponent } from '../sube-imagen/sube-imagen.component';
import { SharedDataService } from '../shared-data.service';
import { Comercio } from '../models';
import {
  AngularFireStorage,
  AngularFireStorageReference,
  AngularFireUploadTask,
} from '@angular/fire/storage';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-nvo-comer',
  templateUrl: './nvo-comer.component.html',
  styleUrls: ['./nvo-comer.component.scss'],
})
export class NvoComerComponent {
  nvoComerForm = this.fb.group({
    nombre: ['', Validators.required],
    ciudad: ['', Validators.required],
    calle: ['', Validators.required],
    nro: ['', Validators.required],
    tel: [''],
    cel: [''],
    rubro: [''],
    logo: [''],
    metPago: this.fb.group({
      tc: [false, []],
      td: [false, []],
      ft: [true, []],
      mp: [false, []],
    }),
    metDeli: this.fb.group({
      moto: [true, []],
      retir: [false, []],
    }),
    pedirPor: this.fb.group({
      whatsapp: [false, []],
      telefono: [true, []],
    }),
  });
  rubros = [];
  cities = [];
  public nvo: boolean;
  fileToUpload: File;
  imgURL: string;
  comer: Comercio = {};
  ref: AngularFireStorageReference;
  downloadURL: Observable<string>;
  loading: boolean = false;
  grabando: boolean = false;

  constructor(
    private fb: FormBuilder,
    private fs: FirestoreService,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private sds: SharedDataService,
    private router: Router
  ) {
    this.fs.getRubros().subscribe((r) => {
      this.rubros = r;
    });
    this.fs.getCiudades().subscribe((c) => {
      this.cities = c;
    });
    if (this.sds.nvoComer) {
      this.nvo = true;
    } else {
      this.loading = true;
      this.llenaFormu();
      this.nvoComerForm.controls['ciudad'].disable();
    }
  }

  openDialog(): void {
    this.sds.archSube = this.nvoComerForm.value.nombre.replace(/ /g, '');
    /* const dialogRef =  */ this.dialog.open(SubeImagenComponent, {
      width: '65%',
      height: '90%',
    });
    /*         dialogRef.afterClosed().subscribe((result) => {
      this.fileToUpload = result;
    }); */
  }

  llenaFormu() {
    this.fs.getComer().subscribe((c) => {
      this.nvoComerForm.setValue(c);
      this.loading = false;
    });
  }

  openSnackBar(msg: string, icn: string) {
    this.snackBar.openFromComponent(SnackBarComponent, {
      data: { msg, icn },
      duration: 2000,
    });
  }

  onSubmit() {
    this.grabando = true;
    if (this.sds.archSube) {
      this.upload(this.sds.archivo);
    } else this.onUploadData();
  }

  upload(archi: File) {
    //se crea nombre archivo quitando los espacios con una regexp
    const narch = `${this.nvoComerForm.value.nombre.replace(
      / /g,
      ''
    )}_logo.png`;
    this.ref = this.storage.ref(`/comercios/${this.sds.userId}/${narch}`);
    try {
      this.ref.put(archi).then(() => {
        this.ref.getDownloadURL().subscribe((url) => {
          this.imgURL = url;
          this.openSnackBar('Se guardó exitósamente bien', 'thumb_up_alt');
          this.onUploadData();
        });
      });
    } catch (error) {
      console.log(error);
    }
  }

  onUploadData() {
    //agrega el campo imagen
    this.comer = { ...this.comer, ...this.nvoComerForm.value };
    if (this.imgURL) this.comer.logo = this.imgURL;

    if (!this.sds.nvoComer) {
      this.fs.updateCom(this.comer);
      this.openSnackBar('Se actualizó el comercio', 'thumb_up');
      this.grabando = false;
      this.sds.archSube = '';
      this.sds.nvoComer = false;
      this.router.navigate(['admin-com']); //lo crea en firestore
    } else {
      this.sds.ciudad = this.nvoComerForm.value.ciudad;
      this.fs.creaCom(this.comer);
      this.openSnackBar('Se creó el comercio', 'thumb_up');
      this.grabando = false;
      this.sds.archSube = '';
      this.sds.nvoComer = false;
      this.router.navigate(['nvoProd']); //lo crea en firestore
    }
  }

  resetModel() {
    this.comer = null;
    this.nvoComerForm.reset();
    this.sds.archSube = null;
  }
}
