import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NvoComerComponent } from './nvo-comer.component';

describe('NvoComerComponent', () => {
  let component: NvoComerComponent;
  let fixture: ComponentFixture<NvoComerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NvoComerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NvoComerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
